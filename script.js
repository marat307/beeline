let menuLinkDrop = document.querySelectorAll('.menu-link-drop'),
    subMenuLink = document.querySelector('.sub-menu-link'),
    dropDown = document.querySelectorAll('.sub-menu-list');

for(var k in menuLinkDrop) {
    menuLinkDrop[k].addEventListener('click', () =>  {
        for(var i in k) {
            dropDown[i].classList.add('sub-menu-list-active');
            if (dropDown[i].style.display == "block") {
                dropDown[i].style.display = "none";
            } else {
                dropDown[i].style.display = "block";
            }
        }
    });
}

// menuLinkDrop[0].addEventListener('click', function upload() {
//     if (dropDown[1].style.display == "block") {
//         dropDown[1].style.display = "none";
//     } else {
//         dropDown[1].style.display = "block";
//     }
// });

